# Stamping.io

## BLOCKCHAIN FOR THE REGISTRATION OF DIGITAL EVIDENCE 
Our platform allows the record of any document and/or data in the chain of blocks in an immutable and permanent way, possessing cryptographic mechanisms that it allows to demonstrate that data series have existed and they were not altered from a specific instant in the time, with legal value of a TSA.

It project contains the necessary libraries in PHP for the registration and validation of transactions to the blockchain of stamping.io

# Installations

* Clone the project:  `git clone https://gitlab.com/stamping/stamping.io.git`
* Go to www.stamping.io/login and register.
* Enter the option:  `Setting->Generate` a token.
* Create a PHP program to register your transactions in the Blockhains of Stamping.io 

The following example explains how to use our libraries in PHP:

## Example (Send a Hash)

```php
<?php
header('Access-Control-Allow-Origin: *');
include 'stampclass.php';

$data = new stampData;
$data->evidence=$_GET['evidence'];
$data->subject="Example generate by stamp/testsave";
$data->transactionType="Stamping.io|testsave";

$stamp = new stamp; 
$stamp->token="MTUzOTA0NDUwNjUyMjp5UFlIS2c5eTh2MW5JMW5TSFl2Zzl4bVhoYWc=";

$result =  $stamp->setStamp($data);


echo ('<h3>Respuesta del Servicio</h3>');
if ($result->code==202) 
    echo ('Mensaje: '.$result->message.'<br> Stamping id:'.$result->trxid.'<br> Code Error: '.$result->code.'<br> URL: <a href=">'.$result->url.'"> '.$result->url.'</a>');
else
    echo ('Mensaje: '.$result->message.'<br> Code Error: '.$result->code);

?>

```

This example allows you to send a SHA256 hash to the blockchain of Stamping.io

## Class: stampResult 

It allows to receive the data before being transferred to the blockchain of stamping.io, this class contains the following properties:

Name        | Descripctions
------------ | -------------
trxid       | Identifier of the transaction that allows to find the record in the Blockchain
code       | code error, example: 200 is Ok
message       | Message of error, if the transaction is ok, the message is: Ok
url | URL that show the register in the Blockchains
evidence       | It contains the main hash of type sha256 that will be anchored in the blockchain of bitcoin, Ethereum and in the private network of Stamping.io
hash2       | Contains a secondary hash value of type sha256 that will be registered in the private network of Stamping.io
hash3       | It contains another sha256 hash that will be registered in the private network of Stamping.io
data        | It contains data that the user wants to associate with the registry, it supports in JSON format, XML or free format.
to       | Identifier of the user who owns the transaction or registered document.
reference   | Free code that you can associate with the transaction.
subject      | Title or brief description of the transaction
transactionType   | Type of transaction, it is recommended to enter a text that describes your application and the type of registration. Example: web.stampig.io|document
from  | Identifier of the user that generated the token.
status| Status of registers.  Endosado (Endorsed) or Anclado (Anchored) .
timestamp  | Date and time of registration in database (UNIX format).
blockchaiId || dentifier of register in Bitcoin, Ethereum and Stamping.io.
blockchainTimestamp  | Date and time of registration in blockchain (UNIX format).
blockchainToken  | Token of blockchain in Bitcoin, Ethereum and Stamping.io.
merkleRootBTC  | Root of the merkler tree that was anchored in BTC.
prefixBTC  | Prefix used in  the merkler tree that was anchored in BTC.
sourceIdBTC  | Source Id used in  the merkler tree that was anchored in BTC.
typeBTC  | Type of registers in BTC.
merkleRootETC  | Root of the merkler tree that was anchored in ETC.
prefixETC  | Prefix used in  the merkler tree that was anchored in ETC.
sourceIdETC  | Source Id used in  the merkler tree that was anchored in ETC.
typeETC  | Type of registers in ETC.
merkleRootETH  | Root of the merkler tree that was anchored in ETH.
prefixETH  | Prefix used in  the merkler tree that was anchored in ETH.
sourceIdETH  | Source Id used in  the merkler tree that was anchored in ETH.
typeETH  | Type of registers in ETH.
merkleRootLTC  | Root of the merkler tree that was anchored in LTC.
prefixLTC  | Prefix used in  the merkler tree that was anchored in LTC.
sourceIdLTC  | Source Id used in  the merkler tree that was anchored in LTC.
typeLTC  | Type of registers in LTC.
merkleRootSTP  | Root of the merkler tree that was anchored in STP.
prefixSTP  | Prefix used in  the merkler tree that was anchored in STP.
sourceIdSTP  | Source Id used in  the merkler tree that was anchored in STP.
typeSTP  | Type of registers in STP.

## Class: stampData 

It allows to receive the data before being transferred to the blockchain of stamping.io, this class contains the following properties:

Name        | Descripctions
------------ | -------------
evidence       | It contains the main hash of type sha256 that will be anchored in the blockchain of bitcoin, Ethereum and in the private network of Stamping.io
hash2       | Contains a secondary hash value of type sha256 that will be registered in the private network of Stamping.io
hash3       | It contains another sha256 hash that will be registered in the private network of Stamping.io
data        | It contains data that the user wants to associate with the registry, it supports in JSON format, XML or free format.
to       | Identifier of the user who owns the transaction or registered document.
reference   | Free code that you can associate with the transaction.
resume      | Title or brief description of the transaction
transactionType   | Type of transaction, it is recommended to enter a text that describes your application and the type of registration. Example: web.stampig.io|document

### Example (Send a Hash)
```php
<?php
    include 'stampclass.php';

    $data = new stampData;
    $data->evidence="281b6657c64072aa811cf79d80ab05ba26d7c6f930e62235b60f77f450e54893";
    $data->hash2="5fef2ab39c2ed60ca5e5dc2b8677861d7e35bff95f6200c58e5965bbfce57c65";
    $data->hash3="6d2b8b5f4e38357e391e03303f6cbcebdb189f140f60ed10abfdc5024220349a";
    $data->owner="alzawcd7c6f930e62235b60f77f450e";
    $data->reference="FAC-1038-2018";
    $data->subject="A test transaction From GitLab";
    $data->transactionType="Stamping.io|FromGitLab";
    $data->data="{url:http://m.download.com/sSHDahT}";
    
    $stamp = new stamp; 
    $stamp->token="MTUzOTA0NDUwNjUyMjp5UFlIS2c5eTh2MW5JMW5TSFl2Zzl4bVhoYWc=" // Enter your token, you can generate a token from http://www.stamping.io/login;
    $result = $stamp->setStamp($data);
    
    if ($result->code=="200") {
        echo ($result->trxid); //Ok
    } else {
        echo ($result->message);
    }

?>
```

## Class: stamp 

Allows to anchor or search data in the blockchain of stampig.io, this class contains the following properties:

Name        | Descripctions
------------ | -------------
token       | Access token that identifies the application or user.  You can generate a token from http://www.stamping.io/login;

This class has the following methods:

Name        | Descripctions | Parameters
------------ | ------------- | -------------
setStamp       | Anchor an event, transaction or document to the blockchain of stamping.io (Bitcoin, Ethereum and Private Network) | _stampData_: Data that will be registered
getStamp       | Show a event, transaction or document | _type_: type of search (byRefrence, byHash, byTrxId, byData) and _value_: Value to search according to the selected search type

### Example (Get a Stamp)

```php
<?php
include 'stampclass.php';

$data = new stampData;
$data->evidence="dc8645864168dcd088d0f4f4cf62951ef92b8a15";
$stamp = new stamp; 
$stamp->token="MTUzOTA0NDUwNjUyMjp5UFlIS2c5eTh2MW5JMW5TSFl2Zzl4bVhoYWc=";


//Type:  byTrxid or byHash;
$result =  $stamp->getStamp("byTrxid",sha1($data->evidence));

if ($result->code=="200") {
        echo ("trxid=".$result->trxid."<br>"); 
        echo ("code=".$result->code."<br>"); 
        echo ("message=".$result->message."<br>"); 
        echo ("url=".$result->url."<br>"); 
        echo ("data=".$result->data."<br>"); 
        echo ("evidence=".$result->evidence."<br>");
        echo ("hash2=".$result->hash2."<br>");
        echo ("hash3=".$result->hash3."<br>");
        echo ("to=".$result->to."<br>");
        echo ("reference=".$result->reference."<br>");
        echo ("subject=".$result->subject."<br>");
        echo ("transactionType=".$result->transactionType."<br>");
        echo ("from=".$result->userId."<br>");
        echo ("status=".$result->status."<br>");
        echo ("timestamp=".$result->timestamp."<br>");
        echo ("blockchaiId=".$result->blockchaiId."<br>");
        echo ("blockchainTimestamp=".$result->blockchainTimestamp."<br>");
        echo ("blockchainToken=".$result->blockchainToken."<br>");

        echo ("merkleRootBTC=".$result->merkleRootBTC."<br>");
        echo ("prefixBTC=".$result->prefixBTC."<br>");
        echo ("sourceIdBTC=".$result->sourceIdBTC."<br>");
        echo ("typeBTC=".$result->typeBTC."<br>");

        echo ("merkleRootETC=".$result->merkleRootETC."<br>");
        echo ("prefixETC=".$result->prefixETC."<br>");
        echo ("sourceIdETC=".$result->sourceIdETC."<br>");
        echo ("typeETC=".$result->typeETC."<br>");

        echo ("merkleRootETH=".$result->merkleRootETH."<br>");
        echo ("prefixETH=".$result->prefixETH."<br>");
        echo ("sourceIdETH=<a href='https://etherscan.io/tx/".$result->sourceIdETH."' target='_blank'>".$result->sourceIdETH."</a><br>");
        echo ("typeETH=".$result->typeETH."<br>");

        echo ("merkleRootLTC=".$result->merkleRootLTC."<br>");
        echo ("prefixLTC=".$result->prefixLTC."<br>");
        echo ("sourceIdLTC=".$result->sourceIdLTC."<br>");
        echo ("typeLTC=".$result->typeLTC."<br>");

        echo ("merkleRootSTP=".$result->merkleRootSTP."<br>");
        echo ("prefixSTP=".$result->prefixSTP."<br>");
        echo ("sourceIdSTP=".$result->sourceIdSTP."<br>");
        echo ("typeSTP=".$result->typeSTP."<br>");

    } else {
        echo ($result->code);
        echo ($result->message);
    }

?>
```

#Notes

* You can show a transaction in Stamping used this URL:
 `http://www.stamping.io/i/?<trxid>&lang=<en: English|es: Spanish>.`

* You can show a transaction in Ethereum used this URL:

`https://etherscan.io/tx/<sourceIdETH>
https://gastracker.io/tx/<sourceIdETC>`


* You can show a transaction in Bitcoin used this URL:

`https://www.blocktrail.com/BTC/tx/<sourceIdBTC>
https://live.blockcypher.com/ltc/tx/<sourceIdLTC>`



For Example:
http://www.stamping.io/i/?684c1410d45337a72e2537160fae3c6838482ac7&lang=en


[stamping.io](http://www.stamping.io/indexEng.html)
Easy, Sure and Confidential

