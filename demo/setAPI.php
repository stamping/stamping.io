<?php

        $ch = curl_init();
        $evidence="cab514ec63e84beb86c89162dd0cd5c622867a30eb965d8341d2602598ca5a53";
        $data = array('evidence' => $evidence);

        curl_setopt($ch, CURLOPT_URL, "https://api.stamping.io/stamp/?evidence=".$evidence);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: Basic MTUzNzc2MDg2Nzk1NjpRaEJCdEZUb2NnYVMwZXNpMWZRNlo4ZTJzTkU="
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        echo ($response);
?>